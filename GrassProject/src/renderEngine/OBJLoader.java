package renderEngine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.lwjgl.util.vector.Vector3f;

import models.RawModel;

public class OBJLoader {
	
	public static RawModel loadObjModel(String fileName, Loader loader) {

		// Get materials

		FileReader fr = null;

		try {
			fr = new FileReader(new File("res/" + fileName + ".mtl"));
		} catch (FileNotFoundException e) {
			System.err.println("Could not read mtl file!");
			e.printStackTrace();
		}

		BufferedReader reader = new BufferedReader(fr);
		String line;

		Map<String, Vector3f> materialsMap = new HashMap<String, Vector3f>();

		try {

			String materialName = "";
			while ((line = reader.readLine()) != null) {
				String[] currentLine = line.split(" ");
				if (line.startsWith("newmtl ")) {
					materialName = currentLine[1];
				} else if (line.startsWith("Kd ")) {
					Vector3f materialColour = new Vector3f(Float.parseFloat(currentLine[1]),
							Float.parseFloat(currentLine[2]), Float.parseFloat(currentLine[3]));
					materialsMap.put(materialName, materialColour);
				}
			}
			reader.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		// Get object

		try {
			fr = new FileReader(new File("res/" + fileName + ".obj"));
		} catch (FileNotFoundException e) {
			System.err.println("Could not read file!");
			e.printStackTrace();
		}

		reader = new BufferedReader(fr);

		List<Vector3f> vertices = new ArrayList<Vector3f>();
		List<Vector3f> normals = new ArrayList<Vector3f>();

		List<Float> verticesArray = new ArrayList<Float>();
		List<Float> normalsArray = new ArrayList<Float>();
		List<Float> coloursArray = new ArrayList<Float>();

		try {

			while (true) {
				line = reader.readLine();
				String[] currentLine = line.split(" ");
				if (line.startsWith("v ")) {
					Vector3f vertex = new Vector3f(Float.parseFloat(currentLine[1]), Float.parseFloat(currentLine[2]),
							Float.parseFloat(currentLine[3]));
					vertices.add(vertex);
				} else if (line.startsWith("vn ")) {
					Vector3f normal = new Vector3f(Float.parseFloat(currentLine[1]), Float.parseFloat(currentLine[2]),
							Float.parseFloat(currentLine[3]));
					normals.add(normal);
				} else if (line.startsWith("usemtl ")) {
					break;
				}
			}

			String currentMaterial = "";
			
			while (line != null) {
				
				if (!line.startsWith("f ")) {
					if (line.startsWith("usemtl ")) {
						currentMaterial = line.split(" ")[1];
					}
					line = reader.readLine();
					continue;
				}
				
				String[] currentLine = line.split(" ");
				String[] vertex1 = currentLine[1].split("//");
				String[] vertex2 = currentLine[2].split("//");
				String[] vertex3 = currentLine[3].split("//");

				Vector3f materialColour = materialsMap.get(currentMaterial);

				processVertex(vertex1, vertices, normals, verticesArray, normalsArray, coloursArray,
						materialColour);
				processVertex(vertex2, vertices, normals, verticesArray, normalsArray, coloursArray,
						materialColour);
				processVertex(vertex3, vertices, normals, verticesArray, normalsArray, coloursArray,
						materialColour);
				
				line = reader.readLine();
				
			}
			reader.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		float[] verticesFinalArray = new float[verticesArray.size()];
		for(int i=0; i < verticesFinalArray.length; i++){
			verticesFinalArray[i] = verticesArray.get(i);
		}
		float[] normalsFinalArray = new float[normalsArray.size()];
		for(int i=0; i < normalsFinalArray.length; i++){
			normalsFinalArray[i] = normalsArray.get(i);
		}
		int[] indicesFinalArray = new int[verticesFinalArray.length/3];
		for(int i=0; i < verticesFinalArray.length/3; i++){
			indicesFinalArray[i] = i;
		}
		float[] coloursFinalArray = new float[coloursArray.size()];
		for(int i=0; i < coloursFinalArray.length; i++){
			coloursFinalArray[i] = coloursArray.get(i);
		}
		
		return loader.loadToVAOMaterial(verticesFinalArray, normalsFinalArray, indicesFinalArray, coloursFinalArray);

	}

	private static void processVertex(String[] vertexData, List<Vector3f> vertices, List<Vector3f> normals,
			List<Float> verticesArray, List<Float> normalsArray, List<Float> coloursArray,
			Vector3f materialColour) {
		
		Vector3f currentVertex = vertices.get(Integer.parseInt(vertexData[0]) - 1);
		verticesArray.add(currentVertex.x);
		verticesArray.add(currentVertex.y);
		verticesArray.add(currentVertex.z);
		
		Vector3f currentNormal = normals.get(Integer.parseInt(vertexData[1]) - 1);
		normalsArray.add(currentNormal.x);
		normalsArray.add(currentNormal.y);
		normalsArray.add(currentNormal.z);
		
		coloursArray.add(materialColour.x);
		coloursArray.add(materialColour.y);
		coloursArray.add(materialColour.z);

	}

}
