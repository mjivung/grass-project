package renderEngine;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
 
import entities.Entity;
import models.RawModel;
import models.TexturedModel;
import shaders.GrassShader;
import toolBox.Maths;
 
public class GrassRenderer {
	
    public GrassRenderer(GrassShader shader, Matrix4f projectionMatrix){
    	shader.start();
        shader.loadProjectionMatrix(projectionMatrix);
        shader.stop();
    }
 
    public void render(Entity entity, GrassShader shader) {
        
    	TexturedModel texturedModel = entity.getTexturedModel();
    	RawModel rawModel = entity.getTexturedModel().getModel();
    	
        GL30.glBindVertexArray(rawModel.getVaoID());
        GL20.glEnableVertexAttribArray(0);
        GL20.glEnableVertexAttribArray(1);
        GL20.glEnableVertexAttribArray(2);
        
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texturedModel.getTexture().getID());
        
        Matrix4f transformationMatrix = Maths.createTransformationMatrix(entity.getPosition(),
                entity.getRotX(), entity.getRotY(), entity.getRotZ(), entity.getScale());
        shader.loadTransformationMatrix(transformationMatrix);
        
        GL11.glDrawArrays(GL11.GL_POINTS, 0, rawModel.getVertexCount());
        
        GL20.glDisableVertexAttribArray(0);
        GL20.glDisableVertexAttribArray(1);
        GL20.glDisableVertexAttribArray(2);
        GL30.glBindVertexArray(0);
        
    }
   
}