package renderEngine;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import shaders.GrassShader;
import skybox.SkyboxRenderer;
import soil.Soil;
import soil.SoilShader;
import terrain.Terrain;
import terrain.TerrainShader;
import entities.Camera;
import entities.Entity;
import entities.Light;

public class MasterRenderer {

	public static final float FOV = 70;
	public static final float NEAR_PLANE = 0.1f;
	public static final float FAR_PLANE = 1000;
	
	private static final float RED = 150/255.0f;
	private static final float GREEN = 213/255.0f;
	private static final float BLUE = 248/255.0f;
	
	private Matrix4f projectionMatrix;
	
	private GrassShader grassShader = new GrassShader();
	private TerrainShader terrainShader = new TerrainShader();
	private SoilShader soilShader = new SoilShader();

	private GrassRenderer grassRenderer;
	private TerrainRenderer terrainRenderer;
	private SoilRenderer soilRenderer;
	private SkyboxRenderer skyboxRenderer;
	
	private List<Terrain> terrains = new ArrayList<Terrain>();
	
	public MasterRenderer(Camera camera, Loader loader){
		//enableCulling();
		createProjectionMatrix();
		terrainRenderer = new TerrainRenderer(terrainShader, projectionMatrix);
		soilRenderer = new SoilRenderer(soilShader, projectionMatrix);
		grassRenderer = new GrassRenderer(grassShader, projectionMatrix);
		skyboxRenderer = new SkyboxRenderer(loader, projectionMatrix);
	}
	
	public void renderScene(List<Entity> entities, Terrain terrain, List<Soil> soils, Light sun, Camera camera, float t, Vector4f[] gustPosAndDir){
		
		prepare();
		
		grassShader.start();
		grassShader.loadViewMatrix(camera);
		grassShader.loadLight(sun);
		grassShader.loadT(t);
		grassShader.loadGustPossitionAndDirection(gustPosAndDir);
		for(Entity entity : entities){
			grassRenderer.render(entity, grassShader);
		}
		grassShader.stop();
		
		terrains.add(terrain);
		terrainShader.start();
		terrainShader.loadLight(sun);
		terrainShader.loadViewMatrix(camera);
		terrainRenderer.render(terrains);
		terrainShader.stop();
		
		soilShader.start();
		soilShader.loadLight(sun);
		soilShader.loadViewMatrix(camera);
		soilRenderer.render(soils);
		soilShader.stop();
		
		skyboxRenderer.render(camera);
		
		terrains.clear();
	}
	
	public void prepare() {
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT|GL11.GL_DEPTH_BUFFER_BIT);
		GL11.glClearColor(RED, GREEN, BLUE, 1);
		//GL13.glActiveTexture(GL13.GL_TEXTURE0);
	}
	
	public static void enableCulling(){
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glCullFace(GL11.GL_BACK);
	}
	
	public static void disableCulling(){
		GL11.glDisable(GL11.GL_CULL_FACE);
	}
	
	private void createProjectionMatrix(){
    	projectionMatrix = new Matrix4f();
		float aspectRatio = (float) Display.getWidth() / (float) Display.getHeight();
		float y_scale = (float) ((1f / Math.tan(Math.toRadians(FOV / 2f))));
		float x_scale = y_scale / aspectRatio;
		float frustum_length = FAR_PLANE - NEAR_PLANE;

		projectionMatrix.m00 = x_scale;
		projectionMatrix.m11 = y_scale;
		projectionMatrix.m22 = -((FAR_PLANE + NEAR_PLANE) / frustum_length);
		projectionMatrix.m23 = -1;
		projectionMatrix.m32 = -((2 * NEAR_PLANE * FAR_PLANE) / frustum_length);
		projectionMatrix.m33 = 0;
    }
	
	public Matrix4f getProjectionMatrix(){
		return projectionMatrix;
	}
	
	public void cleanUp(){
		grassShader.cleanUp();
		terrainShader.cleanUp();
		soilShader.cleanUp();
	}
	
}
