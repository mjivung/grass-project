#version 400 core

in vec3 surfaceNormal;
in vec3 passToLightVector;
in float x;
in vec2 textureCoord;
in vec3 passToCameraVector;

out vec4 out_Colour;

uniform sampler2D textureSampler;

void main(void){
	
	vec3 unitNormal = normalize(surfaceNormal);
	vec3 unitLightVector = normalize(passToLightVector);
	
	vec3 grassColour = vec3(139/255.0, 255/255.0, 0/255.0);
	
	vec3 textureColour = texture(textureSampler, textureCoord).xyz;
	
	float verticalShade = max(0.3, x);
	
	float nDot1 = dot(unitNormal, unitLightVector);
	float brightness = max(nDot1, 0.0)+0.2;
	vec3 diffuse = brightness * textureColour * verticalShade * 2.5;
	
	out_Colour = vec4(diffuse, 1.0);

}