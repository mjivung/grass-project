#version 150

layout ( points ) in;
layout ( triangle_strip, max_vertices = 50 ) out;

in vec3 toLightVector[];
in vec2 passDisplacement[];
in vec2 passSize[];

out vec3 surfaceNormal;
out vec3 passToLightVector;
out float x; 
out vec2 textureCoord;

uniform mat4 modelMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform vec3 lightPosition;
uniform float t;
uniform vec4 gust_1;
uniform vec4 gust_2;
uniform vec4 gust_3;
uniform vec4 gust_4;
uniform vec4 gust_5;


vec3 interpolate(in vec3 startPosition, in vec3 endPosition, in float t);
vec3 getGustEffect(in float gustStrength,in float gustThreshold,in float iFact,in float gustPowAddition,
in float baseToGustDist1,in float baseToGustDist2,in float baseToGustDist3,in float baseToGustDist4,in float baseToGustDist5,
in vec3 gust_direction_1,in vec3 gust_direction_2,in vec3 gust_direction_3,in vec3 gust_direction_4,in vec3 gust_direction_5);

void main(void){

	float sections = 5;

	passToLightVector = toLightVector[0];

	mat4 mvpMatrix = projectionMatrix * viewMatrix * modelMatrix;

	vec3 basePosition = gl_in[0].gl_Position.xyz;
	vec3 worldBasePosition = (modelMatrix * vec4(basePosition, 1.0)).xyz;
	
	float width = 0.15 * passSize[0].x;
	float height = 5 * passSize[0].y;
	
	vec3 gustPosition1= vec3(gust_1.x,0,gust_1.y);
	vec3 gustPosition2= vec3(gust_2.x,0,gust_2.y);
	vec3 gustPosition3= vec3(gust_3.x,0,gust_3.y);
	vec3 gustPosition4= vec3(gust_4.x,0,gust_4.y);
	vec3 gustPosition5= vec3(gust_5.x,0,gust_5.y);
		
	vec3 gust_direction_1= vec3(gust_1.z,0,gust_1.w);
	vec3 gust_direction_2= vec3(gust_2.z,0,gust_2.w);
	vec3 gust_direction_3= vec3(gust_3.z,0,gust_3.w);
	vec3 gust_direction_4= vec3(gust_4.z,0,gust_4.w);
	vec3 gust_direction_5= vec3(gust_5.z,0,gust_5.w);
	
	float baseToGustDist1 = length(worldBasePosition-gustPosition1);
	float baseToGustDist2 = length(worldBasePosition-gustPosition2);
	float baseToGustDist3 = length(worldBasePosition-gustPosition3);
	float baseToGustDist4 = length(worldBasePosition-gustPosition4);
	float baseToGustDist5 = length(worldBasePosition-gustPosition5);	
	
	float topXDisplacement = passDisplacement[0].x;
	float topZDisplacement = passDisplacement[0].y;	
	
	vec3 leftBottomPosition = basePosition + vec3(-width, 0, 0);
	vec3 rightBottomPosition = basePosition + vec3(width, 0, 0);
	vec3 topPosition = basePosition;
	
	// WIND
	
	float windStrength = 0.5;
	float gustStrength = 25;
	float gustThreshold = 3000;	
	float iFact=11;
	float gustPowAddition=0.015;																								//Gust Wind
	//float maxGustEffect=3;
	topPosition=topPosition-getGustEffect(gustStrength,gustThreshold,iFact,gustPowAddition, baseToGustDist1, baseToGustDist2, baseToGustDist3, baseToGustDist4, baseToGustDist5,
 gust_direction_1, gust_direction_2, gust_direction_3, gust_direction_4, gust_direction_5);
	
	float minorWindPower = 0.5 + sin(worldBasePosition.x + worldBasePosition.z + t*(3+windStrength/20));										//Base Wind
	float majorWindPower = 0.5 + sin(worldBasePosition.x/30 + worldBasePosition.z/30 + t*(1.0+windStrength/20));
	
	if(minorWindPower < 0.0){
		minorWindPower = minorWindPower*0.3;
	} else {
		minorWindPower = minorWindPower*0.13;
	}
	
	majorWindPower = majorWindPower*0.5;
	
	minorWindPower = minorWindPower*windStrength;
	majorWindPower = majorWindPower*windStrength;
	
	vec3 windDirection = vec3(1.0, 0.0, 0.0);
	
	topPosition = topPosition + vec3(topXDisplacement, height, topZDisplacement) + windDirection*minorWindPower + windDirection*majorWindPower;
	
	// Surface normal
	vec3 bottomVector = (modelMatrix * vec4(rightBottomPosition, 1.0) - modelMatrix * vec4(leftBottomPosition, 1.0)).xyz;
	surfaceNormal = cross(bottomVector, vec3(0.0,1.0,0.0));

 	x = 0;
 	
	textureCoord = vec2(0,0);
 	gl_Position = mvpMatrix * vec4(leftBottomPosition, 1.0);
	EmitVertex();
	
	textureCoord = vec2(1,0);
	gl_Position = mvpMatrix * vec4(rightBottomPosition, 1.0);
	EmitVertex();
 
 	for(int i=0; i<sections; i++){
		 	
		x += 1/sections;
		
		textureCoord = vec2(0,0);
		vec3 position = interpolate(leftBottomPosition, topPosition, x);
		gl_Position = mvpMatrix * vec4(position, 1.0);
		EmitVertex();
		
		if(i != sections-1){ 
			textureCoord = vec2(1,0);
			position = interpolate(rightBottomPosition, topPosition, x);
			gl_Position = mvpMatrix * vec4(position, 1.0);
			EmitVertex();
		}
		 	
 	}
 	
 	EndPrimitive();

}

vec3 getGustEffect(in float gustStrength,in float gustThreshold,in float iFact,in float gustPowAddition,
in float baseToGustDist1,in float baseToGustDist2,in float baseToGustDist3,in float baseToGustDist4,in float baseToGustDist5,
in vec3 gust_direction_1,in vec3 gust_direction_2,in vec3 gust_direction_3,in vec3 gust_direction_4,in vec3 gust_direction_5){
	vec3 gustEffect= vec3(0.0,0.0,0.0);
	bool notPrevConputed =true;
	float gustPow=1;
	for(int i=0;(i < gustThreshold) && notPrevConputed ;i++){
	
		if(baseToGustDist1<i ){
			float gustScale1=1/(pow(baseToGustDist1*(1+i/iFact),gustPow));
			 gustEffect=gustEffect+(normalize(gust_direction_1)*gustStrength*gustScale1);
			 notPrevConputed=false;
		}	
		
		if( baseToGustDist2<i){
			float gustScale2=1/(pow(baseToGustDist2*(1+i/iFact),gustPow));
			gustEffect=gustEffect+normalize(gust_direction_2)*gustStrength*gustScale2;
			notPrevConputed=false;
		}
			
		if(baseToGustDist3<i ){
			float gustScale3=1/(pow(baseToGustDist3*(1+i/iFact),gustPow));
			gustEffect=gustEffect+normalize(gust_direction_3)*gustStrength*gustScale3;
			notPrevConputed=false;
		}
			
		if(baseToGustDist4<i ){
			float gustScale4=1/(pow(baseToGustDist4*(1+i/iFact),gustPow));
			gustEffect=gustEffect+normalize(gust_direction_4)*gustStrength*gustScale4;
			notPrevConputed=false;
		}
	
		if(baseToGustDist5<i ){
			float gustScale5=1/(pow(baseToGustDist5*(1+i/iFact),gustPow));
			gustEffect=gustEffect+normalize(gust_direction_5)*gustStrength*gustScale5; 
			notPrevConputed=false;
		}	
		
		///* gustEffect= vec3(min(gustEffect.x,maxGustEffect),0,max(gustEffect.x,-maxGustEffect));											//code might be needed for maxEffect
		//float topXPos=  min(max(topPosition.x-gustEffect.x,topPosition.x-maxGustEffect),topPosition.x+maxGustEffect);
		//float topZPos= min(max(topPosition.z-gustEffect.z,topPosition.z-maxGustEffect),topPosition.z+maxGustEffect);
		//float topYPos= topPosition.y-(topXPos+topZPos)/10;
		//topPosition= vec3(topXPos,topYPos,topZPos);*/
		
		gustPow=gustPow+gustPowAddition;	
	}
	return gustEffect;
		
}

vec3 interpolate(in vec3 startPosition, in vec3 endPosition, in float x){
		
	vec3 startTangent = vec3(0.0, 1.0, 0.0);
	vec3 endTangent = vec3(endPosition.x*2, 0, endPosition.z*2);
	
	vec3 position = startPosition * (2*pow(x,3)-3*pow(x,2)+1);
	position += startTangent * (pow(x,3)-2*pow(x,2)+x);
	position += endPosition * (-2*pow(x,3)+3*pow(x,2));
	position += endTangent * (pow(x,3)-pow(x,2));
	
	return position;
		
}