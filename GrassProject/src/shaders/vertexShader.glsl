#version 150

in vec3 position;
in vec2 displacement;
in vec2 size;

out vec3 toLightVector;
out vec2 passDisplacement;
out vec2 passSize;

uniform mat4 modelMatrix;
uniform vec3 lightPosition;

void main(void){

	vec4 worldPosition = modelMatrix * vec4(position, 1.0);

	toLightVector = lightPosition - worldPosition.xyz;
	
	passDisplacement = displacement;
	
	passSize = size;
	
	gl_Position = vec4(position,1.0);

}