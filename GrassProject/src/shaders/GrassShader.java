package shaders;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import toolBox.Maths;

import entities.Camera;
import entities.Light;

public class GrassShader extends ShaderProgram{
	
	private static final String VERTEX_FILE = "src/shaders/vertexShader.glsl";
	private static final String GEOMETRY_FILE = "src/shaders/geometryShader.glsl";
	private static final String FRAGMENT_FILE = "src/shaders/fragmentShader.glsl";
	
	private int location_modelMatrix;
	private int location_projectionMatrix;
	private int location_viewMatrix;
	private int location_lightPosition;
	private int location_t;
	private int location_gust_1;
	private int location_gust_2;
	private int location_gust_3;
	private int location_gust_4;
	private int location_gust_5;
	
	public GrassShader() {
		super(VERTEX_FILE, GEOMETRY_FILE, FRAGMENT_FILE);
	}

	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
		super.bindAttribute(1, "displacement");
		super.bindAttribute(2, "growth");
	}

	@Override
	protected void getAllUniformLocations() {
		location_modelMatrix = super.getUniformLocation("modelMatrix");
		location_projectionMatrix = super.getUniformLocation("projectionMatrix");
		location_viewMatrix = super.getUniformLocation("viewMatrix");
		location_lightPosition = super.getUniformLocation("lightPosition");
		location_t = super.getUniformLocation("t");
		location_gust_1 = super.getUniformLocation("gust_1");
		location_gust_2 = super.getUniformLocation("gust_2");
		location_gust_3 = super.getUniformLocation("gust_3");
		location_gust_4 = super.getUniformLocation("gust_4");
		location_gust_5 = super.getUniformLocation("gust_5");
	}
	
	public void loadTransformationMatrix(Matrix4f matrix){
		super.loadMatrix(location_modelMatrix, matrix);
	}
	
	public void loadViewMatrix(Camera camera){
		Matrix4f viewMatrix = Maths.createViewMatrix(camera);
		super.loadMatrix(location_viewMatrix, viewMatrix);
	}
	
	public void loadProjectionMatrix(Matrix4f projection){
		super.loadMatrix(location_projectionMatrix, projection);
	}
	
	public void loadLight(Light light){
		super.loadVector(location_lightPosition, light.getPosition());
	}
	
	public void loadT(float t){
		super.loadFloat(location_t, t);
	}

	public void loadGustPossitionAndDirection(Vector4f[] gustDirection) {
		super.loadVector(location_gust_1, gustDirection[0]);
		super.loadVector(location_gust_2, gustDirection[1]);
		super.loadVector(location_gust_3, gustDirection[2]);
		super.loadVector(location_gust_4, gustDirection[3]);
		super.loadVector(location_gust_5, gustDirection[4]);
	}
	
}
