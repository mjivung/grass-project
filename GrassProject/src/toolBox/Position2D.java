package toolBox;

import entities.Entity;

public class Position2D {

	private int x;
	private int z;
	
	public Position2D(int x,  int z){
		this.x = x;
		this.z = z;
	}

	public int getX() {
		return x;
	}

	public int getZ() {
		return z;
	}
	
	public boolean isAtSameLocation(Entity entity){
		if(x == entity.getPosition().x && z == entity.getPosition().z){
			return true;
		}
		return false;
	}
	
}
