package entities;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector3f;

import renderEngine.DisplayManager;
import terrain.Terrain;

public class Camera {

	private static final int MOVE_SPEED = 20;
	private static final int ZOOM_SPEED = 200;
	private static final int ZOOM_FAR_LIMIT = 50;	
	
	private Vector3f position = new Vector3f(0,0,0);
	private float pitch = 30;
	private float yaw;
	private float roll;
	private Terrain terrain;
	
	public Camera(int x, int y, int z, Terrain terrain){
		position.x = x;
		position.y = y;
		position.z = z;
		this.terrain = terrain;
	}

	public void move(){
		checkInputs();
		calculatePitch(); 
	}
	
	public Vector3f getPosition() {
		return position;
	}
	
	public Vector3f getDirection(){
		Vector3f direction = new Vector3f(
				(float) (Math.cos(Math.toRadians(pitch)) *  Math.sin(Math.toRadians(yaw))),
				(float) Math.sin(Math.toRadians(pitch)),
				(float) (Math.cos(Math.toRadians(pitch)) *  Math.cos(Math.toRadians(yaw)))
		);
		direction.normalise();
		return direction;
	}

	public float getPitch() {
		return pitch;
	}

	public float getYaw() {
		return yaw;
	}

	public float getRoll() {
		return roll;
	}
	
	public void invertPitch(){
		this.pitch = -pitch;
	}
	
	private void checkInputs(){
	
		float frameTimeSeconds = DisplayManager.getFrameTimeSeconds();
		if(Keyboard.isKeyDown(Keyboard.KEY_W)){
			position.z -= MOVE_SPEED * frameTimeSeconds;
		} else if(Keyboard.isKeyDown(Keyboard.KEY_S)){
			position.z += MOVE_SPEED * frameTimeSeconds;
		} 
		if(Keyboard.isKeyDown(Keyboard.KEY_A)){
			position.x -= MOVE_SPEED * frameTimeSeconds;
		} else if(Keyboard.isKeyDown(Keyboard.KEY_D)){
			position.x += MOVE_SPEED * frameTimeSeconds;
		}
		
		Vector3f zoom = getDirection();
		zoom.scale(DisplayManager.getFrameTimeSeconds());
		zoom.scale(ZOOM_SPEED);
		int wheel = Mouse.getDWheel();
		float terrainY = terrain.getHeightOfTerrain(position.x, position.z);
		float minY = terrainY + 2;
		if(wheel < 0 && position.y < ZOOM_FAR_LIMIT){
			position = Vector3f.add(position, zoom, position);
			if(position.y > ZOOM_FAR_LIMIT){ position.y = ZOOM_FAR_LIMIT; }
		} else if(wheel > 0 && position.y > minY){
			position = Vector3f.sub(position, zoom, position);
		}
		if(position.y < minY){
			position.y = minY;
		}

	}
	
	private void calculatePitch(){
		float maxPitch = 90;
		float minPitch = -30;
		if(Mouse.isButtonDown(1) && pitch >= minPitch && pitch <= maxPitch){
			float pitchChange = Mouse.getDY() * 0.1f;
			pitch -= pitchChange;
		}
		if(pitch < minPitch){ pitch = minPitch; }
		if(pitch > maxPitch){ pitch = maxPitch; }
	}
	
	/*private void calculateYaw(){
		if(Mouse.isButtonDown(0)){
			float angleChange = Mouse.getDX() * 0.3f;
			yaw -= angleChange;
		}
	}*/
	
}
