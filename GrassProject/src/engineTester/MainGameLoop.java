package engineTester;

import java.util.ArrayList;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import entities.Camera;
import entities.Entity;
import entities.Light;
import models.RawModel;
import models.TexturedModel;
import renderEngine.DisplayManager;
import renderEngine.Loader;
import renderEngine.MasterRenderer;
import soil.Soil;
import terrain.Terrain;
import textures.ModelTexture;

public class MainGameLoop {

	public static void main(String[] args) {

		DisplayManager.createDisplay();
		
		Loader loader = new Loader();
		
		// Terrain
		Terrain terrain = new Terrain(0, -1, loader, "heightmap_flat");
		
		// Front soil
		ArrayList<Soil> soils = new ArrayList<Soil>();
		Soil frontSoil = new Soil(0, -8, loader);
		soils.add(frontSoil);
		
		// Left soil
		Soil leftSoil = new Soil(0, -8, loader);
		leftSoil.setRotY(90);
		soils.add(leftSoil);
		
		// Right soil
		Soil rightSoil = new Soil(0, -8, loader);
		rightSoil.increaseXPos(terrain.getSize());
		rightSoil.setRotY(90);
		soils.add(rightSoil);
		
		ArrayList<Entity> entities = new ArrayList<Entity>();

		float[] grassBladeBaseVertices = {0.0f, 0.0f, 0.0f};
		
		ModelTexture texture = new ModelTexture(loader.loadTexture("grass"));

		// Create grass blades
		for(int x=0; x<terrain.getSize()-1; x++){
			for(int z=0; z<terrain.getSize()-1; z++){
				for(int i=0; i<4; i++){
					float[] displacement = {(float) (Math.random()*2-1), (float) (Math.random()*2-1)};
					float[] size = {(float) (0.5+Math.random()), (float) (0.5+Math.random())};
					float xOffset = (float) (Math.random());
					float zOffset = (float) (Math.random());
					float xPos = x+xOffset+0.2f;
					float zPos = -z-zOffset-0.2f;
					float yPos = terrain.getHeightOfTerrain(xPos, zPos);
					RawModel model = loader.loadToVAOPoints(grassBladeBaseVertices, displacement, size);
					TexturedModel texturedModel = new TexturedModel(model, texture);
					Entity point = new Entity(texturedModel, new Vector3f(xPos, yPos, zPos), 0, 0, 0, 1);
					point.increaseRotation(0, (float)Math.random()*90, 0);
					entities.add(point);
				}
			}
		}
		
		Camera camera = new Camera((int) terrain.getSize() / 2, 20, 5, terrain);
		
		MasterRenderer masterRenderer = new MasterRenderer(camera, loader);
		
		Light sun = new Light(new Vector3f(-50, 50, 50), new Vector3f(1, 1, 1));
		
		// GUST
		
		float t = 0;
		float gustInterpols[] = new float[5];
		
		for (int i = 0; i < gustInterpols.length; i++) {
			gustInterpols[i]=(float) Math.random();
			//System.out.println("Interpolstart for "+i+" = "+gustInterpols[i]);
		}
		
		float bottomGustValue=100;
		float gustHeight=10f;
		float topGustValue=-100;
		float gustUnitDist=20;
		
		Vector3f[] gustStartPosition= new Vector3f[5];
		Vector3f[] gustEndPosition= new Vector3f[5];
		Vector3f[] gustDirection= new Vector3f[5];
		
		for (int i = 0; i < gustInterpols.length; i++) {
			gustStartPosition[i]=setNewStartPoints(gustStartPosition, gustUnitDist, bottomGustValue,gustHeight);
			gustEndPosition[i]=setNewEndPoints(gustEndPosition, gustUnitDist, topGustValue, gustHeight);
			gustDirection[i]=Vector3f.sub(gustStartPosition[i], gustEndPosition[i], gustDirection[i]);
		}

		Vector3f[] gustPosition=new Vector3f[5];
		
		// LOOP
		
		while (!Display.isCloseRequested()) {

			for (int i = 0; i < gustInterpols.length; i++) {
				if((gustInterpols[i]/5)%1<=0.01){
					gustStartPosition[i]=setNewStartPoints(gustStartPosition, gustUnitDist, bottomGustValue,gustHeight);
					gustEndPosition[i]=setNewEndPoints(gustEndPosition, gustUnitDist, topGustValue, gustHeight);
					gustDirection[i]=Vector3f.sub(gustStartPosition[i], gustEndPosition[i], gustDirection[i]);
				}
			}
			
			for (int i = 0; i < gustPosition.length; i++) {
				gustPosition[i] = interpolate(gustStartPosition[i],gustEndPosition[i],(gustInterpols[i])%1);
			}
			for (int i = 0; i < gustInterpols.length; i++) {
				gustInterpols[i] = (float) (gustInterpols[i]+0.1*DisplayManager.getFrameTimeSeconds());
			}
			
			t += 0.05;
			
			Vector4f[] gustPosAndDir = new Vector4f[5];
			for (int i = 0; i < gustPosAndDir.length; i++) {
				gustPosAndDir[i]= new Vector4f(gustPosition[i].x,gustPosition[i].z,gustDirection[i].x,gustDirection[i].z);
			}
			
			camera.move();
			
			masterRenderer.renderScene(entities, terrain, soils, sun, camera, t, gustPosAndDir);
			
			DisplayManager.updateDisplay();

		}
		
		masterRenderer.cleanUp();
		loader.cleanUp();
		DisplayManager.closeDisplay();

	}
	
	private static Vector3f setNewStartPoints(Vector3f[] gustStartPosition,float gustUnitDist, float bottomGustVal,float gustHeight){
		 
		float gustStartRand=(float) Math.random();																																
		float gustXPos=gustUnitDist+Math.max((bottomGustVal+bottomGustVal/2)*(gustStartRand),bottomGustVal/2);
		float gustZPos=bottomGustVal-gustXPos+gustUnitDist;
		
		return new Vector3f(gustXPos,gustHeight,gustZPos);
		
	}
	
	private static Vector3f setNewEndPoints(Vector3f[] gustEndPosition,float gustUnitDist, float topGustVal, float gustHeight){
		
		float gustEndRand=(float) Math.random();
		float gustZPos=Math.min((topGustVal+topGustVal/2)*(gustEndRand),topGustVal/2)-gustUnitDist;
		float gustXPos=-(gustZPos+100)-gustUnitDist;

		return new Vector3f(gustXPos,gustHeight,gustZPos);
		
	}

	private static  Vector3f interpolate(Vector3f startOriginalPosition, Vector3f endOriginalPosition, float x){
		
		Vector3f startPosition= new Vector3f(startOriginalPosition.x,startOriginalPosition.y,startOriginalPosition.z);
		Vector3f endPosition= new Vector3f(endOriginalPosition.x,endOriginalPosition.y,endOriginalPosition.z);
		float randXDir=(float) ((Math.random()*2-1)*0.5);
		float randZDir=(float) ((Math.random()*2-1)*0.5);

		Vector3f startTangent = new Vector3f(-1-randXDir, (float) -0.0, -1-randZDir);

		Vector3f endTangent = new Vector3f(1+randXDir, (float) 0.0, 1+randZDir);
		
		Vector3f position = (Vector3f) (startPosition.scale((float) (2*Math.pow(x,3)-3*Math.pow(x,2)+1)));
		 position = Vector3f.add(position, (Vector3f) (startTangent.scale((float) (Math.pow(x,3)-2*Math.pow(x,2)+x))), position);
		 position = Vector3f.add(position, (Vector3f) (endPosition.scale((float) (-2*Math.pow(x,3)+3*Math.pow(x,2)))), position);
		 position = Vector3f.add(position, (Vector3f) (endTangent.scale((float) (Math.pow(x,3)-Math.pow(x,2)))), position);
			
		return position;
			
	}

}
