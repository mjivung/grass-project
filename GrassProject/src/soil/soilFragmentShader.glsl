#version 400 core

in vec3 surfaceNormal;
in vec3 toLightVector;
in vec2 passTexCoords;
in float yPos;

out vec4 out_Colour;

uniform sampler2D textureSampler;

void main(void){

	vec3 unitNormal = normalize(surfaceNormal);
	vec3 unitLightVector = normalize(toLightVector);

	vec3 textureColour = texture(textureSampler, passTexCoords).xyz;
	
	float y = -1*yPos/8+0.5;
	y = max(0.4, y);

	float nDot1 = dot(unitNormal, unitLightVector);
	float brightness = max(nDot1, 0.0)+0.2;
	vec3 diffuse = brightness * textureColour * y * 0.7;
	
	out_Colour = vec4(diffuse, 1.0);

}