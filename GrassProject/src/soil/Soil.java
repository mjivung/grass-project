package soil;

import models.RawModel;
import models.TexturedModel;
import renderEngine.Loader;
import textures.ModelTexture;

public class Soil {
	
	private final static int WIDTH = 100;
	private final static int HEIGHT = 10;
	
	private TexturedModel textutredModel;
	private float x;
	private float y;
	private float rotX, rotY, rotZ;
	
	public Soil(float x, float y, Loader loader){
		this.textutredModel = generateSoil(loader);
		this.x = x;
		this.y = y;
	}

	public TexturedModel getTexturedModel() {
		return textutredModel;
	}
	
	public float getX(){
		return x;
	}
	
	public float getY(){
		return y;
	}
	
	public void increaseXPos(float dx){
		x += dx;
	}
	
	public float getRotX() {
		return rotX;
	}

	public void setRotX(float rotX) {
		this.rotX = rotX;
	}

	public float getRotY() {
		return rotY;
	}

	public void setRotY(float rotY) {
		this.rotY = rotY;
	}

	public float getRotZ() {
		return rotZ;
	}

	public void setRotZ(float rotZ) {
		this.rotZ = rotZ;
	}

	private TexturedModel generateSoil(Loader loader){
		
		int numXVertices = 11;
		int numYVertices = 2;
		
		int numVertices = numXVertices * numYVertices;
		
		float[] vertices = new float[numVertices * 3];
		float[] normals = new float[numVertices * 3];
		float[] texCoords = new float[numVertices * 2];
		
		int vertexPointer = 0;
		
		for(int i=0;i<numXVertices;i++){
			for(int j=0;j<numYVertices;j++){
				
				vertices[vertexPointer*3] = i*(WIDTH/(numXVertices-1));
				vertices[vertexPointer*3+1] = j*(HEIGHT/(numYVertices-1));
				vertices[vertexPointer*3+2] = 0;
				
				normals[vertexPointer*3] = 0;
				normals[vertexPointer*3+1] = 0;
				normals[vertexPointer*3+2] = 1;

				texCoords[vertexPointer*2] = i%2;
				texCoords[vertexPointer*2+1] = j;
				
				vertexPointer++;
			}
		}
			
		int[] indices = new int[6*(numVertices-1)];
		int pointer = 0;
		for(int x=0;x<numXVertices-1;x++){
			int bottomLeft = x*2;
			int topLeft = bottomLeft + 1;
			int bottomRight = topLeft + 1;
			int topRight = bottomRight + 1;
			indices[pointer++] = bottomLeft;
			indices[pointer++] = topRight;
			indices[pointer++] = topLeft;
			indices[pointer++] = bottomLeft;
			indices[pointer++] = bottomRight;
			indices[pointer++] = topRight;
		}
		
		RawModel rawModel = loader.loadToVAOSoil(vertices, normals, indices, texCoords);
		
		ModelTexture texture = new ModelTexture(loader.loadTexture("soil"));
		
		return new TexturedModel(rawModel, texture);
		
	}
	
}
