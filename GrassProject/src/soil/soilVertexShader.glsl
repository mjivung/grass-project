#version 400 core

in vec3 position;
in vec3 normal;
in vec2 texCoords;

out vec3 surfaceNormal;
out vec3 toLightVector;
out vec2 passTexCoords;
out float yPos;


uniform mat4 transformationMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform vec3 lightPosition;

void main(void){

	passTexCoords = texCoords;

	vec4 worldPosition = transformationMatrix * vec4(position, 1.0);
	
	yPos = worldPosition.y;
	
	gl_Position = projectionMatrix * viewMatrix * transformationMatrix * vec4(position, 1.0);
	
	surfaceNormal = (transformationMatrix * vec4(normal, 0.0)).xyz;
	toLightVector = lightPosition - worldPosition.xyz;

}